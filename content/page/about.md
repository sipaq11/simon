---
title: À propos de moi
# subtitle: Why you'd want to go on a date with me
comments: false
---

Mon nom est Simon Paquette. Je suis professeur de mathématiques au [Cégep Édouard-Montpetit](https://www.cegepmontpetit.ca/). J'adore les mathématiques, \\(\LaTeX{}\\) et la programmation.

